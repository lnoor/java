/**
 * Created by Lisandra on 16.11.2016.
 */

//Kaare andmete defineerimine
public class Kaar {
    public Tipp algus;
    public Tipp lopp;
    public int langus;

    public Kaar (Tipp alg, Tipp lop, int lan){
        algus = alg ;
        lopp = lop;
        langus = lan;
    }
}