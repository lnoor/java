/**
 * Created by Lisandra on 16.11.2016.
 */

import java.util.ArrayList;

/**
 * Olgu antud geograafiliste punktide graaf, mille iga kaarega (x, y)
 * on seotud väli (x, y).l - langus liikumisel punktist x punkti y.
 * Ohutuimaks teeks kahe punkti vahel nimetatakse sellist teed
 * millel suurim lokaalne langus (punktist järgmisse punkti) on
 * minimaalne. Kirjutada graafi laiuti läbimisel põhinev algoritm,
 * mille käigus leitakse ohutuim tee antud punktist a antud punkti b.
 */
public class home6 {
    public static void main (String[] param){
        Kaar[] kaared;

        //kaare x koordinaar, y koordinaat ja pikkus
        kaared = new Kaar[16];
        kaared[0] = new Kaar(new Tipp(12, 11), new Tipp(5, 12), 6);
        kaared[1] = new Kaar(new Tipp(12, 11), new Tipp(6, 1), 11);
        kaared[2] = new Kaar(new Tipp(12, 11), new Tipp(12, 4), 9);
        kaared[3] = new Kaar(new Tipp(5, 12), new Tipp(5, 7), 3);
        kaared[4] = new Kaar(new Tipp(5, 12), new Tipp(12, 11), 6);
        kaared[5] = new Kaar(new Tipp(5, 7), new Tipp(6, 1), 7);
        kaared[6] = new Kaar(new Tipp(5, 7), new Tipp(5, 12), 3);
        kaared[7] = new Kaar(new Tipp(6, 1), new Tipp(12, 4), 5);
        kaared[8] = new Kaar(new Tipp(6, 1), new Tipp(12, 11), 11);
        kaared[9] = new Kaar(new Tipp(6, 1), new Tipp(5, 7), 7);
        kaared[10] = new Kaar(new Tipp(12, 4), new Tipp(12, 11), 9);
        kaared[11] = new Kaar(new Tipp(12, 4), new Tipp(5, 7), 8);
        kaared[12] = new Kaar(new Tipp(12, 4), new Tipp(6, 1), 5);
        kaared[13] = new Kaar(new Tipp(12, 11), new Tipp(6, 1), 11);
        kaared[14] = new Kaar(new Tipp(12, 11), new Tipp(12, 4), 9);
        kaared[15] = new Kaar(new Tipp(12, 11), new Tipp(5, 12), 6);

        //Programmi testimine
        Tipp algus = new Tipp (12, 4);
        Tipp lopp = new Tipp (12, 11);

        Kaar k = kaareOtsimine(algus, algus, kaared);

        //süsteemi testimine
        //System.out.println(Integer.toString(k.algus.x1) + ","  + Integer.toString(k.algus.y1));
        //System.out.println(Integer.toString(k.lopp.x1) + ","  + Integer.toString(k.lopp.y1));

        //Andmete sisestamine
        ArrayList<Tipp> tipud = new ArrayList<>();
        tipud.add(algus);

        while (k.lopp.x1 != lopp.x1 || k.lopp.y1 != lopp.y1){

            k = kaareOtsimine(k.lopp, k.algus, kaared);

            tipud.add(k.algus);
        }
        tipud.add(lopp);
        for (Tipp t : tipud) {

           System.out.println(Integer.toString(t.x1) + ","  + Integer.toString(t.y1));
        }
    }
    public static Kaar kaareOtsimine(Tipp alg, Tipp lop, Kaar[] kaared ) {

        //kaare andmete defineerimine
        int langus = 0;
        int s = 0;
        Kaar b = null;

        //lühima tee leidmine
        for (int i = 0; i < kaared.length; i++) {
            if (kaared[i].algus.x1 == alg.x1 && kaared[i].algus.y1 == alg.y1
                    && (kaared[i].lopp.x1 != lop.x1 || kaared[i].lopp.y1 != lop.y1)){
                if (s == 0){
                    //süsteemi testimine
                    //System.out.println(Integer.toString(kaared[i].algus.x1) + ","  + Integer.toString(kaared[i].algus.y1));
                   //System.out.println(Integer.toString(kaared[i].lopp.x1) + ","  + Integer.toString(kaared[i].lopp.y1));
                    //System.out.println(Integer.toString(kaared[i].langus));
                    langus = kaared[i].langus;
                    b = kaared[i];
                    s = 1;
                } else {
                    if (kaared[i].langus < langus) {
                        //süsteemi testimine
                        //System.out.println(Integer.toString(kaared[i].algus.x1) + ","  + Integer.toString(kaared[i].algus.y1));
                        //System.out.println(Integer.toString(kaared[i].lopp.x1) + ","  + Integer.toString(kaared[i].lopp.y1));
                        //System.out.println(Integer.toString(kaared[i].langus));
                        langus = kaared[i].langus;
                        b = kaared[i];
                    }
                }
            }
        }
        return b;
    }
}